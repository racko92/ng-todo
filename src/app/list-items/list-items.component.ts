import { AuthService } from './../auth.service';
import { Component, Input, Output } from '@angular/core';
import { IListItem } from '../list-items/list-items';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss']
})
export class ListItemsComponent {
  constructor(private authService: AuthService) {}

  todoList: IListItem[] = [
    {id: 1, text: 'Create todo array'},
    {id: 2, text: 'Display it\'s content in template'}
  ];

  newItem: string = '';

  addNewItem(): void {
    if (this.newItem) {
      this.todoList.push({id: this.todoList.length + 1, text: this.newItem});
      this.newItem = '';
    }
  }

  deleteItem(item: IListItem): void {
    this.todoList = this.todoList.filter(listItem => listItem.id !== item.id);
  }


  disableButton(): boolean {
    return this.newItem.length < 1 ? true : false;
  }

  logout(): void {
    this.authService.logout();
  }
}
