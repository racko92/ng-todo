import { User } from './auth/user.model';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  users: User[] = [];

  loggedInStatus: boolean = sessionStorage.getItem('UserEmail') ? true : false;

  constructor(private router: Router) {
    const user = new User('example@email.com', 'letMeIn');
    this.users.push(user);
  }

  getUsers() {
    return this.users;
  }

  login(user: User) {
    const userLoggingIn = this.users.find(u => u.email === user.email && u.password === user.password);

    this.router.navigate(['/todo']);

    return userLoggingIn
      ? ((this.loggedInStatus = true ) && sessionStorage.setItem('UserEmail', user.email))
      : ((this.loggedInStatus = false) && sessionStorage.removeItem('UserEmail'));
  }

  registerUser(email: string, password: string) {
    this.users.push(new User(email, password));
    console.log(this.users);
  }

  logout() {
    this.loggedInStatus = false;
    sessionStorage.removeItem('UserEmail');
    this.router.navigate(['/login']);
  }
}
