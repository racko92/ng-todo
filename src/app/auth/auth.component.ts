import { AuthService } from './../auth.service';
import { User } from './user.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

function passwordValidator(c: AbstractControl): { [key: string]: boolean } | null {
  if (c.value.replace( /^\D+/g, '').length < 1) {
    return { 'noNumeric': true };
  }
  return null;
}

function passwordCompare(c: AbstractControl): { [key: string]: boolean } | null {
    const passwordControl = c.get('password');
    const passwordRepeatControl = c.get('repeatPassword');

    if (passwordControl.pristine || passwordRepeatControl.pristine) {
      return null;
    }

    if (passwordControl.value === passwordRepeatControl.value) {
      return null;
    }

    return { 'match': true };
  }

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  users: User[];

  user: User = new User('', '');

  switchForms: boolean = false;

  registrationForm: FormGroup;

  registrationUser: User = new User('', '');

  emailMessage: string;

  private validationMessages = {
    required: 'Please enter your email address',
    email: 'Please enter a valid email address'
  };

  constructor(public authService: AuthService, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.registrationForm = this.fb.group({
      email: ['', [Validators.required, Validators.email, Validators.minLength(3)]],
      passwordGroup: this.fb.group({
        password: ['', passwordValidator],
        repeatPassword: ['', [Validators.required, Validators.minLength(3)]]
      }, { validator: passwordCompare})
    });

    this.users = this.authService.getUsers();

    const emailControl = this.registrationForm.get('email');
    emailControl.valueChanges.pipe(
      debounceTime(500)
    ).subscribe(
      value => this.setMessage(emailControl)
    );
  }

  submit() {
    this.authService.login(this.user);
  }

  switchForm() {
    this.switchForms = !this.switchForms;
  }

  registerUser() {
    const email = this.registrationForm.get('email').value;
    const password = this.registrationForm.get('passwordGroup.password').value;
    this.authService.registerUser(email, password);
  }

  setMessage(c: AbstractControl): void {
    this.emailMessage = '';
    if ((c.touched || c.dirty) && c.errors) {
      this.emailMessage += Object.keys(c.errors).map(key => this.emailMessage = this.validationMessages[key]);
    }
  }
}
